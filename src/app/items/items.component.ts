import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from '../services/authenticate.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { ItemsService } from '../services/items.service';

declare var $: any;

@Component({
  selector: 'app-items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {
  showMenu = false;
  page = 0;
  data = [];

  constructor(private auth: AuthenticateService,
              private spinner: NgxSpinnerService,
              private shop: ItemsService) {}
  ngOnInit() {
    $('#waterfall').NewWaterfall({
      width: 200,
      delay: 60,
      repeatShow: false
  });
    this.auth.currentMessage.subscribe(message => this.showMenu = message);
    this.getProducts();
  }
  getProducts() {
    this.spinner.show();
    this.shop.getItems(this.page).subscribe((res: any) => this.onSuccess(res));
  }
  onSuccess(res) {
    console.log(res);
    if (res !== undefined) {
      this.spinner.hide();
      res.forEach((item: any) => {
        this.data.push(item);
      });
    }
  }
  onScroll() {
    this.page = this.page + 12;
    this.getProducts();
  }
}
