import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ItemsService } from '../services/items.service';
import { AuthenticateService } from '../services/authenticate.service';


@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.css']
})
export class ItemDetailsComponent implements OnInit {
  url: string;
  error: string;
  itemInfo: any;
  showMenu = false;
  constructor(private route: ActivatedRoute, 
              private spinner: NgxSpinnerService,
              private item: ItemsService,
              private auth: AuthenticateService) { }

  ngOnInit() {
      this.spinner.show();
      this.auth.currentMessage.subscribe(message => this.showMenu = message);
      this.route.paramMap.subscribe(params => {
          this.getItem(params.get('id'));
      });
  }
  getItem(id) {
    this.spinner.hide();
    this.item.getItem(id).subscribe(
      resp => {
        this.spinner.hide();
        this.itemInfo = resp;
      }, err => {
        this.spinner.hide();
      });
  }
  procure() {
  }

}
