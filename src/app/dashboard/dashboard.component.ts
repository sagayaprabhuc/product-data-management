import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from '../services/authenticate.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  showMenu = false;
  constructor(private auth: AuthenticateService) { }
  ngOnInit() {
    this.auth.currentMessage.subscribe(message => this.showMenu = message);
  }

}
