import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};
@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  constructor(private http: HttpClient) { }

  getItems(page: number): Observable <any> {
    return this.http.get<any>(environment.endPoint + 'items?start=' + page + '&count=12');
  }
  scrape(url): Observable <any> {
    return this.http.get<any>(environment.endPoint + 'scrape?url=' + url);
  }
  getItem(id): Observable <any> {
    return this.http.get<any>(environment.endPoint + 'brewDetails/' + id);
  }
}
