import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from '../services/authenticate.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  showMenu = false;
  constructor(private auth: AuthenticateService) { }
  toggleMenu() {
    this.showMenu =  !this.showMenu;
    this.auth.changeMessage(this.showMenu);
  }
  ngOnInit() {
    this.auth.currentMessage.subscribe(message => this.showMenu = message);
  }

}
