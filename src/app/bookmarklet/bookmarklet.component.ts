
import { Component, OnInit } from '@angular/core';
import { AuthenticateService } from '../services/authenticate.service';

@Component({
  selector: 'app-bookmarklet',
  templateUrl: './bookmarklet.component.html',
  styleUrls: ['./bookmarklet.component.css']
})
export class BookmarkletComponent implements OnInit {

  showMenu = false;
  constructor(private auth: AuthenticateService) { }
  ngOnInit() {
    this.auth.currentMessage.subscribe(message => this.showMenu = message);
  }
}
